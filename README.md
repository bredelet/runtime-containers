# Runtime Containers
This repository contains all Runtime containerization reference architectures, guidance, and definitions.
## Contents

[Amazon Elastic Kubernetes Service - Boomi Molecule](https://bitbucket.org/officialboomi/runtime-containers/src/master/eks/)

[Kubernetes Reference Architecture - Boomi Molecule](https://bitbucket.org/officialboomi/runtime-containers/src/master/Kubernetes/)

